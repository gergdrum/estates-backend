package estates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEstateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootEstateApplication.class, args);
    }
}
