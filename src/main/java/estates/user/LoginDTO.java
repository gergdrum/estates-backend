package estates.user;

import lombok.Builder;
import lombok.Data;

@Data
public class LoginDTO {
    private String userName;
    private String password;
}
