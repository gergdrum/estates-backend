package estates.user;

import estates.general.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserDTO getAccountById(Long id) {
        return convertToUserDTO(userRepository.getOne(id));
    }

    public List<UserDTO> getUsers() {
        List<User> users = userRepository.findAll();
        return users.stream().map(this::convertToUserDTO).collect(Collectors.toList());
    }

    private UserDTO convertToUserDTO(User user) {

        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setPassword(user.getPassword());
        userDTO.setUserName(user.getUserName());
        userDTO.setRole(user.getRole().toString());
        return userDTO;
    }

    private User convertToUser(UserDTO userDTO) {
        return User.builder()
                .id(null)
                .password(passwordEncoder.encode(userDTO.getPassword()))
                .userName(userDTO.getUserName())
                .role(Role.valueOf(userDTO.getRole()))
                .build();

    }

    public UserDTO login(LoginDTO loginData) {

        Optional<User> user = userRepository.findByUserName(loginData.getUserName());
        if (user.isPresent()) {
            if (passwordEncoder.matches(loginData.getPassword(), user.get().getPassword())) {
                System.out.println("HttpStatus.ok");
                return convertToUserDTO(user.get());
            } else {
                return null;
            }
        } else {
            System.out.println("HttpStatus.NOT_FOUND");
            return null;
        }
    }

    public void createUser(UserDTO userDTO) {
        userRepository.save(convertToUser(userDTO));
    }

    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }
}
